import Home from '~/pages/Home';

// No need login still access
const publicRoutes = [{ path: '/', component: Home }];

// Need login
const privateRoutes = [];

export { publicRoutes, privateRoutes };
