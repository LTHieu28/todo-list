import { useEffect, useReducer, useRef } from 'react';
import reducer, { initState } from './reducer';
import {
  addTask,
  checkedBox,
  displayDetail,
  // onchangeDescriptionDetail,
  onchangeDuedateDetail,
  // onchangeNameDetail,
  onchangePiorityDetail,
  removeTask,
  removeTasks,
  search,
  updateDetail,
} from './actions';
import { Button, DatePicker, Dropdown, Menu, Space, Checkbox } from 'antd';
import { useState } from 'react';
import { DownOutlined } from '@ant-design/icons';
import './index.scss';
import moment from 'moment';

function Home() {
  let initDueDate = moment(new Date().toJSON()).format('YYYY-MM-DD');
  const [stateTotal, dispatch] = useReducer(reducer, initState);
  const [newTask, setNewTask] = useState('');
  const [description, setDescription] = useState('');
  const [dueDate, setDueDate] = useState(initDueDate);
  const [piority, setPiority] = useState('Normal');
  const [displayWarn, setDisplayWarn] = useState('none');
  const [displayWarnDetail, setDisplayWarnDetail] = useState('none');
  const [displayBulk, setDisplayBulk] = useState('none');
  const [countCheckedBoxes, setCountCheckedBoxes] = useState(0);
  const [whichChecked, setWhichChecked] = useState([]);
  const [searchTask, setSearchTask] = useState('');
  const newTaskRef = useRef();
  const searchRef = useRef();
  // const detailTaskRef = useRef();
  // const detailDescriptionRef = useRef();

  const { state } = stateTotal;

  useEffect(() => {
    dispatch(search(searchTask));
    searchRef.current.focus();
  }, [searchTask]);

  useEffect(() => {
    const updateChecked = () => {
      let temp_checked = [];
      state.map((task, i) => {
        if (task.checkedBox === true) {
          temp_checked.push(i);
        }
        setWhichChecked(temp_checked);
        return null;
      });
    };
    updateChecked();
  }, [state]);

  const handleMenuClick = (e) => {
    switch (e.key) {
      case '1':
        setPiority('Low');
        break;
      case '2':
        setPiority('Normal');
        break;
      case '3':
        setPiority('High');
        break;
      default:
        setPiority('Normal');
    }
  };

  const menu = (
    <Menu
      onClick={handleMenuClick}
      items={[
        {
          label: 'Low',
          key: '1',
        },
        {
          label: 'Normal',
          key: '2',
        },
        {
          label: 'High',
          key: '3',
        },
      ]}
    />
  );

  const menuDetail = (index) => (
    <Menu
      onClick={(e) => {
        switch (e.key) {
          case '1':
            dispatch(onchangePiorityDetail({ index, newPiority: 'Low' }));
            break;
          case '2':
            dispatch(onchangePiorityDetail({ index, newPiority: 'Normal' }));
            break;
          case '3':
            dispatch(onchangePiorityDetail({ index, newPiority: 'High' }));
            break;
          default:
            dispatch(onchangePiorityDetail({ index, newPiority: 'Normal' }));
        }
      }}
      items={[
        {
          label: 'Low',
          key: '1',
        },
        {
          label: 'Normal',
          key: '2',
        },
        {
          label: 'High',
          key: '3',
        },
      ]}
    />
  );

  const disabledDate = (current) => {
    // Can not select days before today and today
    return current && current <= moment().endOf('day');
  };

  const addNewTask = () => {
    if (newTask !== '') {
      const payload = { task: newTask, description, dueDate, piority, checkedBox: false, displayDetail: 'none' };
      dispatch(addTask(payload));
      dispatch(search(searchTask));
      // searchRef.current.focus();
      setNewTask('');
      setDescription('');
      setDueDate(moment(new Date().toJSON()).format('YYYY-MM-DD'));
      setPiority('Normal');
      setDisplayWarn('none');
    } else {
      setDisplayWarn('block');
      newTaskRef.current.focus();
    }
  };

  const Task = ({ task, index }) => {
    return (
      <div className="task">
        <div className="header-task">
          <Checkbox
            onChange={(e) => {
              if (e.target.checked) {
                setDisplayBulk('flex');
                setCountCheckedBoxes(countCheckedBoxes + 1);
              } else {
                const newCountCheckedBoxes = countCheckedBoxes - 1;
                if (newCountCheckedBoxes === 0) setDisplayBulk('none');
                setCountCheckedBoxes(newCountCheckedBoxes);
              }
              const payload = { index, status: e.target.checked };
              dispatch(checkedBox(payload));
            }}
            className="checkBox"
            checked={state[index].checkedBox}
          >
            {task.task}
          </Checkbox>
          <button
            className="btn-detail"
            onClick={() => {
              setDisplayWarnDetail('none');
              let payload = {};
              switch (state[index].displayDetail) {
                case 'none':
                  payload = { index, style: 'flex' };
                  dispatch(displayDetail(payload));
                  break;
                case 'flex':
                  payload = { index, style: 'none' };
                  dispatch(displayDetail(payload));
                  break;
                default:
                  throw new Error('Invalid style.');
              }
            }}
          >
            Detail
          </button>
          <button className="btn-remove" onClick={() => dispatch(removeTask(index))}>
            Remove
          </button>
        </div>
        <div className="detail-task" style={{ display: state[index].displayDetail }}>
          <div className="required-name">
            <h4 className="warning-empty" style={{ display: displayWarnDetail }}>
              This field is required !!
            </h4>
            <input
              // ref={detailTaskRef}
              type="text"
              className="name-task"
              defaultValue={state[index].task}
              id={task.task + index}
              // onChange={async (e) => {
              //   setDisplayWarnDetail('none');
              //   await dispatch(onchangeNameDetail({ index, newName: e.target.value }));
              //   // temp_ref.current.focus();
              // }}
              onChange={async (e) => {
                await setDisplayWarnDetail('none');
                document.getElementById(task.task + index).focus();
              }}
              placeholder="Name task..."
            />
          </div>
          <div className="description">
            <h4 className="description-title">Description</h4>
            <input
              // ref={detailDescriptionRef}
              type="text"
              className="description-text"
              defaultValue={state[index].description}
              id={task.description + index}
              // onChange={async (e) => {
              //   await dispatch(onchangeDescriptionDetail({ index, newDescription: e.target.value }));
              //   // detailDescriptionRef.current.focus();
              // }}
            />
          </div>
          <div className="date-piority">
            <div className="due-date">
              <h4 className="dueDate-title">Due Date</h4>
              <DatePicker
                defaultValue={moment()}
                value={state[index].dueDate !== '' ? moment(state[index].dueDate) : null}
                className="dueDate-picker"
                placeholder="Deadline..."
                disabledDate={disabledDate}
                id={task.dueDate + index}
                onChange={(date, dateString) => {
                  if (dateString !== '') {
                    dispatch(onchangeDuedateDetail({ index, newDate: dateString }));
                  } else {
                    const now = new Date().toJSON().slice(0, 10);
                    dispatch(onchangeDuedateDetail({ index, newDate: now }));
                  }
                }}
              />
            </div>
            <div className="piority">
              <h4 className="piority-title">Piority</h4>
              <Dropdown className="menu-piority" overlay={menuDetail(index)}>
                <Button>
                  <Space>
                    <div className="current-piority">{state[index].piority}</div>
                    <DownOutlined className="dropDown-btn" />
                  </Space>
                </Button>
              </Dropdown>
            </div>
          </div>
          <button
            className="confirm-task"
            onClick={async () => {
              // if (state[index.task] !== '') {
              if (document.getElementById(task.task + index).value !== '') {
                setDisplayWarnDetail('none');
                await dispatch(
                  updateDetail({
                    index,
                    newTask: document.getElementById(task.task + index).value,
                    newDescription: document.getElementById(task.description + index).value,
                  }),
                );
                await dispatch(search(searchTask));
                // searchRef.current.focus();
                alert('Update successfully!');
              } else {
                await setDisplayWarnDetail('block');
                document.getElementById(task.task + index).focus();
              }
            }}
          >
            Update
          </button>
        </div>
      </div>
    );
  };

  return (
    <>
      <div className="wrapper">
        <div className="new-task">
          <h3 className="title-newTask">New Task</h3>
          <div className="required-name">
            <h4 className="warning-empty" style={{ display: displayWarn }}>
              This field is required !!
            </h4>
            <input
              ref={newTaskRef}
              type="text"
              className="name-task"
              value={newTask}
              onChange={(e) => {
                setDisplayWarn('none');
                setNewTask(e.target.value);
              }}
              placeholder="Add new task ..."
            />
          </div>
          <div className="description">
            <h4 className="description-title">Description</h4>
            <input
              type="text"
              className="description-text"
              value={description}
              onChange={(e) => setDescription(e.target.value)}
            />
          </div>
          <div className="date-piority">
            <div className="due-date">
              <h4 className="dueDate-title">Due Date</h4>
              <DatePicker
                defaultValue={moment()}
                value={dueDate !== '' ? moment(dueDate) : null}
                className="dueDate-picker"
                disabledDate={disabledDate}
                placeholder="Deadline..."
                onChange={(date, dateString) => {
                  if (dateString !== '') {
                    setDueDate(dateString);
                  } else {
                    setDueDate(new Date().toJSON().slice(0, 10));
                  }
                }}
              />
            </div>
            <div className="piority">
              <h4 className="piority-title">Piority</h4>
              <Dropdown className="menu-piority" overlay={menu}>
                <Button>
                  <Space>
                    <div className="current-piority">{piority}</div>
                    <DownOutlined className="dropDown-btn" />
                  </Space>
                </Button>
              </Dropdown>
            </div>
          </div>
          <button className="confirm-task" onClick={addNewTask}>
            Add
          </button>
        </div>
        <div className="todo-list">
          <h3 className="title-todoList">To Do List</h3>
          <input
            ref={searchRef}
            type="text"
            className="search"
            value={searchTask}
            onChange={async (e) => {
              setSearchTask(e.target.value);
            }}
            placeholder="Search..."
          />
          <div className="tasks">
            {state.map((task, index) => {
              return <Task key={index} task={task} index={index} />;
            })}
          </div>
          <div className="bulk-action" style={{ display: displayBulk }}>
            <h3 className="title-bulkAction">Bulk Action:</h3>
            <button className="btn-done-bulkAction">Done</button>
            <button
              className="btn-remove-bulkAction"
              onClick={() => {
                dispatch(removeTasks(whichChecked));
                setWhichChecked([]);
                setCountCheckedBoxes(0);
                setDisplayBulk('none');
              }}
            >
              Remove
            </button>
          </div>
        </div>
      </div>
    </>
  );
}

export default Home;
