import {
  ADD_TASK,
  CHECKED_BOX,
  DISPLAY_DETAIL,
  ONCHANGE_DESCRIPTION_DETAIL,
  ONCHANGE_DUEDATE_DETAIL,
  ONCHANGE_NAME_DETAIL,
  ONCHANGE_PIORITY_DETAIL,
  REMOVE_TASK,
  REMOVE_TASKS,
  SEARCH,
  UPDATE_DETAIL,
} from './constants';

export const addTask = (payload) => {
  return {
    type: ADD_TASK,
    payload,
  };
};

export const removeTask = (payload) => {
  return {
    type: REMOVE_TASK,
    payload,
  };
};

export const removeTasks = (payload) => {
  return {
    type: REMOVE_TASKS,
    payload,
  };
};

export const checkedBox = (payload) => {
  return {
    type: CHECKED_BOX,
    payload,
  };
};

export const displayDetail = (payload) => {
  return {
    type: DISPLAY_DETAIL,
    payload,
  };
};

export const updateDetail = (payload) => {
  return {
    type: UPDATE_DETAIL,
    payload,
  };
};

export const onchangeNameDetail = (payload) => {
  return {
    type: ONCHANGE_NAME_DETAIL,
    payload,
  };
};

export const onchangeDescriptionDetail = (payload) => {
  return {
    type: ONCHANGE_DESCRIPTION_DETAIL,
    payload,
  };
};

export const onchangeDuedateDetail = (payload) => {
  return {
    type: ONCHANGE_DUEDATE_DETAIL,
    payload,
  };
};

export const onchangePiorityDetail = (payload) => {
  return {
    type: ONCHANGE_PIORITY_DETAIL,
    payload,
  };
};

export const search = (payload) => {
  return {
    type: SEARCH,
    payload,
  }
}
