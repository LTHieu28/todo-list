import {
  ADD_TASK,
  CHECKED_BOX,
  DISPLAY_DETAIL,
  ONCHANGE_DESCRIPTION_DETAIL,
  ONCHANGE_DUEDATE_DETAIL,
  ONCHANGE_NAME_DETAIL,
  ONCHANGE_PIORITY_DETAIL,
  REMOVE_TASK,
  REMOVE_TASKS,
  SEARCH,
  UPDATE_DETAIL,
} from './constants';

export const initState = {
  anchorState: [],
  state: [],
};

const reducer = (state, action) => {
  switch (action.type) {
    case ADD_TASK:
      let return_anchorState = [];
      let addTask_anchorState = [...state.anchorState, action.payload];
      let dueDates = [];
      for (let task of addTask_anchorState) {
        dueDates.push(task.dueDate);
      }
      dueDates.sort(function (a, b) {
        const date1 = new Date(a);
        const date2 = new Date(b);
        return date1 - date2;
      });
      for (let date of dueDates) {
        addTask_anchorState.map((task, index) => {
          if (task.dueDate === date && !return_anchorState.includes(addTask_anchorState[index])) {
            return_anchorState.push(addTask_anchorState[index]);
          }
          return null;
        });
      }
      return {
        ...state,
        anchorState: return_anchorState,
        state: return_anchorState,
      };

    case CHECKED_BOX:
      let newState_checkedBox = [...state.state];
      let newAnchorState_checkedBox = [...state.anchorState];
      newState_checkedBox[action.payload.index].checkedBox = action.payload.status;
      for (let task of newAnchorState_checkedBox) {
        if (task.task === newState_checkedBox[action.payload.index].task) {
          task.checkedBox = action.payload.status;
        }
      }
      return {
        ...state,
        anchorState: newAnchorState_checkedBox,
        state: newState_checkedBox,
      };

    case DISPLAY_DETAIL:
      let newState_displayDetail = [...state.state];
      let newAnchorState_displayDetail = [...state.anchorState];
      newState_displayDetail[action.payload.index].displayDetail = action.payload.style;
      for (let task of newAnchorState_displayDetail) {
        if (task.task === newState_displayDetail[action.payload.index].task) {
          task.displayDetail = action.payload.style;
        }
      }
      return {
        ...state,
        anchorState: newAnchorState_displayDetail,
        state: newState_displayDetail,
      };

    case UPDATE_DETAIL:
      let return_updateState = [];
      let newState_updateDetail = [...state.anchorState];
      newState_updateDetail[action.payload.index].task = action.payload.newTask;
      newState_updateDetail[action.payload.index].description = action.payload.newDescription;

      let dueDates_update = [];
      for (let task of newState_updateDetail) {
        dueDates_update.push(task.dueDate);
      }
      dueDates_update.sort(function (a, b) {
        const date1 = new Date(a);
        const date2 = new Date(b);
        return date1 - date2;
      });
      for (let date of dueDates_update) {
        newState_updateDetail.map((task, index) => {
          if (task.dueDate === date && !return_updateState.includes(newState_updateDetail[index])) {
            return_updateState.push(newState_updateDetail[index]);
          }
          return null;
        });
      }
      return {
        ...state,
        anchorState: return_updateState,
        state: return_updateState,
      };
    // return newState_updateDetail;

    case ONCHANGE_NAME_DETAIL:
      let newState_nameDetail = [...state.state];
      let newAnchorState_nameDetail = [...state.anchorState];
      newState_nameDetail[action.payload.index].task = action.payload.newName;
      for (let task of newAnchorState_nameDetail) {
        if (task.description === newState_nameDetail[action.payload.index].description) {
          task.task = action.payload.newName;
        }
      }
      return {
        ...state,
        anchorState: newAnchorState_nameDetail,
        state: newState_nameDetail,
      };

    case ONCHANGE_DESCRIPTION_DETAIL:
      let newState_desDetail = [...state.state];
      let newAnchorState_desDetail = [...state.anchorState];
      newState_desDetail[action.payload.index].description = action.payload.newDescription;
      for (let task of newAnchorState_desDetail) {
        if (task.task === newState_desDetail[action.payload.index].task) {
          task.description = action.payload.newDescription;
        }
      }
      return {
        ...state,
        anchorState: newAnchorState_desDetail,
        state: newState_desDetail,
      };

    case ONCHANGE_DUEDATE_DETAIL:
      let newState_dateDetail = [...state.state];
      let newAnchorState_dateDetail = [...state.anchorState];
      newState_dateDetail[action.payload.index].dueDate = action.payload.newDate;
      for (let task of newAnchorState_dateDetail) {
        if (task.task === newState_dateDetail[action.payload.index].task) {
          task.dueDate = action.payload.newDate;
        }
      }
      return {
        ...state,
        anchorState: newAnchorState_dateDetail,
        state: newState_dateDetail,
      };

    case ONCHANGE_PIORITY_DETAIL:
      let newState_piorityDetail = [...state.state];
      let newAnchorState_piorityDetail = [...state.anchorState];
      newState_piorityDetail[action.payload.index].piority = action.payload.newPiority;
      for (let task of newAnchorState_piorityDetail) {
        if (task.task === newState_piorityDetail[action.payload.index].task) {
          task.piority = action.payload.newPiority;
        }
      }
      return {
        ...state,
        anchorState: newAnchorState_piorityDetail,
        state: newState_piorityDetail,
      };

    case REMOVE_TASK:
      let newState_removeTask = [...state.state];
      let newAnchorState_removeTask = [...state.anchorState];
      newState_removeTask.splice(action.payload, 1);
      state.anchorState.map((task, index) => {
        if (task.task === state.state[action.payload].task) newAnchorState_removeTask.splice(index, 1);
        return null;
      });
      return {
        ...state,
        anchorState: newAnchorState_removeTask,
        state: newState_removeTask,
      };

    case REMOVE_TASKS:
      let newState_anchor = [...state.anchorState];
      let newState_removeTasks = [...state.state];
      let index_remove = action.payload;
      let temp;
      for (let i = 0; i < index_remove.length - 1; i++) {
        for (let j = i + 1; j < index_remove.length; j++) {
          if (index_remove[j] > index_remove[i]) {
            temp = index_remove[i];
            index_remove[i] = index_remove[j];
            index_remove[j] = temp;
          }
        }
      }
      for (let i of index_remove) {
        newState_removeTasks.splice(i, 1);
        state.anchorState.map((task, index) => {
          if (task.task === state.state[i].task) newState_anchor.splice(index, 1);
          return null;
        });
      }
      for (let task of newState_removeTasks) {
        task.checkedBox = false;
      }
      return {
        ...state,
        anchorState: newState_anchor,
        state: newState_removeTasks,
      };

    case SEARCH:
      let returnState_search = [];
      let newState_search = [...state.anchorState];
      if (action.payload !== '') {
        for (let task of newState_search) {
          if (task.task.includes(action.payload)) {
            returnState_search.push(task);
          }
        }
      } else {
        returnState_search = [...state.anchorState];
      }
      // return newState_search;
      return {
        ...state,
        state: returnState_search,
      };

    default:
      throw new Error('Invalid action.');
  }
};

export default reducer;
