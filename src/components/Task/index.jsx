import { Button, DatePicker, Dropdown, Menu, Space } from 'antd';
import { useState } from 'react';
import { DownOutlined } from '@ant-design/icons';

function Task({ initTask='', initDescription='', initPiority='Normal', nameButton='Add' }) {
  const [newTask, setNewTask] = useState(initTask);
  const [description, setDescription] = useState(initDescription);
  const [piority, setPiority] = useState(initPiority);

  const handleMenuClick = e => {
    switch (e.key) {
      case '1':
        setPiority('Low');
        break;
      case '2':
        setPiority('Normal');
        break;
      case '3':
        setPiority('High');
        break;
      default:
        setPiority('Normal');
    }
  }

  const menu = (
    <Menu
      onClick={handleMenuClick}
      items={[
        {
          label: 'Low',
          key: '1',
        },
        {
          label: 'Normal',
          key: '2',
        },
        {
          label: 'High',
          key: '3',
        },
      ]}
    />
  );

  return (
    <>
      <input 
        type="text"
        className="name-task"
        value={newTask}
        onChange={e => setNewTask(e.target.value)}
      />
      <input 
        type="text"
        className="description"
        value={description}
        onChange={e => setDescription(e.target.value)}
      />
      <div className='date-piority'>
        <div className='due-date'>
          <DatePicker placeholder='Deadline...' />
        </div>
        <div className='piority'>
          <Dropdown overlay={menu}>
            <Button>
              <Space>
                {piority}
                <DownOutlined />
              </Space>
            </Button>
          </Dropdown>
        </div>
      </div>
      <button className='confirm-task'>{nameButton}</button>
    </>
  )
};

export default Task;
