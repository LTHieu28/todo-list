# Todo List

This project was created by React 5.0.1 , a simple demo web page for Entrance Test Frontend

### Prerequisites

Nodejs version >= 14

You can install nodejs by following link:
https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-20-04

### Step 1. Clone code from git

```bash
cd /your_folder/
git clone https://gitlab.com/LTHieu28/todo-list.git
cd todo-list
```

### Step 2. Installing the libraries

```bash
npm install
```

### Step 3. Running

```bash
npm start
```
